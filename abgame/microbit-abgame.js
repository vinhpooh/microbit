input.onButtonPressed(Button.A, function () {
    if (playable) {
        playable = false;
        aPoint++;
        setRow(aPointRow, drawRowPoint(aPoint));
    }
})
input.onButtonPressed(Button.B, function () {
    if (playable) {
        playable = false;
        bPoint++;
        setRow(bPointRow, drawReverseRowPoint(bPoint));
    }
})
input.onButtonPressed(Button.AB, function () {
    if (aPoint >= 5 || bPoint >= 5) {
        reset();
    }
})
input.onShake(function () {
    launch();
})
function draw() {
    for (let y = 0; y <= leds.length - 1; y++) {
        for (let x = 0; x <= leds[y].length - 1; x++) {
            if (leds[y][x] == 1) {
                led.plot(x, y);
            } else {
                led.unplot(x, y);
            }
        }
    }
}
function launch() {
    for (let index = 0; index < 50; index++) {
        drawRandom();
        draw();
        basic.pause(20);
    }
    clearFlash();
    playable = true;
}
function drawFlash1() {
    setRow(squareRow1, [0, 1, 1, 1, 0]);
    setRow(squareRow2, [0, 1, 0, 1, 0]);
    setRow(squareRow3, [0, 1, 1, 1, 0]);
}
function drawFlash2() {
    setRow(squareRow1, [0, 0, 0, 0, 0]);
    setRow(squareRow2, [0, 0, 1, 0, 0]);
    setRow(squareRow3, [0, 0, 0, 0, 0]);
}
function drawRandom() {
    setRow(squareRow1, [0, random(), random(), random(), 0]);
    setRow(squareRow2, [0, random(), random(), random(), 0]);
    setRow(squareRow3, [0, random(), random(), random(), 0]);
}
function clearFlash() {
    setRow(squareRow1, [0, 0, 0, 0, 0]);
    setRow(squareRow2, [0, 0, 0, 0, 0]);
    setRow(squareRow3, [0, 0, 0, 0, 0]);
}
function drawRowPoint(point: number) {
    let row = [0, 0, 0, 0, 0];
    for (let i = 0; i <= row.length - 1; i++) {
        if (i < point) {
            row[i] = 1;
        }
    }
    return row;
}
function drawReverseRowPoint(point: number) {
    let row = drawRowPoint(point)
    helpers.arrayReverse(row);
    return row;
}
function setRow(index: number, row: number[]) {
    leds[index] = row;
}
function random() {
    let random = Math.floor(Math.random() * Math.floor(2));
    return random;
}
function reset() {
    aPoint = 1;
    bPoint = 1;
    setRow(aPointRow, drawRowPoint(aPoint));
    setRow(bPointRow, drawReverseRowPoint(bPoint));
}
let playable = false;
let aPoint = 1;
let bPoint = 1;
const aPointRow = 4;
const bPointRow = 0;
const squareRow1 = 1;
const squareRow2 = 2;
const squareRow3 = 3;
const leds = [drawReverseRowPoint(bPoint), [0], [0], [0], drawRowPoint(aPoint)];
basic.forever(function () {
    draw();
})