input.onButtonPressed(Button.A, function () {
    tempo *= 1.25;
    if (tempo > 2000) {
        tempo = 2000;
    }
})
input.onButtonPressed(Button.B, function () {
    tempo /= 1.25;
    if (tempo < 0.001) {
        tempo = 0.001;
    }
})
input.onButtonPressed(Button.AB, function () {
    if (range == RANGE_DEFAULT) {
        range = RANGE_CUSTOM;
        let xMax = random(5);
        let xMin = random(xMax + 1);
        let yMax = random(5);
        let yMin = random(yMax + 1);
        RANGES[RANGE_CUSTOM] = [[xMin, xMax], [yMin, yMax]];
    } else {
        range = RANGE_DEFAULT;
    }
})
input.onShake(function () {
    size++;
})
input.onPinPressed(TouchPin.P0, function () {
})
function random(count: number) {
    let random = Math.floor(Math.random() * Math.floor(count));
    return random;
}
function draw(leds: number[][]) {
    helpers.arrayForEach(leds, function (value: number[], index: number) {
        led.plot(value[X], value[Y]);
    });
}
function forward(position: number[], clockwise: boolean, ranges: number[][]) {
    let xMin = ranges[X][MIN];
    let xMax = ranges[X][MAX];
    let yMin = ranges[Y][MIN];
    let yMax = ranges[Y][MAX];
    let newPosition;

    if (clockwise) {
        newPosition = runClockwise(position[X], position[Y], position[HEADING], xMin, xMax, yMin, yMax);
    } else {
        newPosition = runCounterClockwise(position[X], position[Y], position[HEADING], xMin, xMax, yMin, yMax);
    }

    return newPosition;
}
function runClockwise(x: number, y: number, heading: number, xMin: number, xMax: number, yMin: number, yMax: number) {
    switch (heading) {
        case HEADING_EAST:
            x++;
            if (x >= xMax) {
                x = xMax;
                heading = HEADING_SOUTH;
            }
            break;
        case HEADING_SOUTH:
            y++;
            if (y >= yMax) {
                y = yMax;
                heading = HEADING_WEST;
            }
            break;
        case HEADING_WEST:
            x--;
            if (x <= xMin) {
                x = xMin;
                heading = HEADING_NORTH;
            }
            break;
        default:
            y--;
            if (y <= yMin) {
                y = yMin;
                heading = HEADING_EAST;
            }
    }
    return [x, y, heading];
}
function runCounterClockwise(x: number, y: number, heading: number, xMin: number, xMax: number, yMin: number, yMax: number) {
    switch (heading) {
        case HEADING_EAST:
            x++;
            if (x >= xMax) {
                x = xMax;
                heading = HEADING_NORTH;
            }
            break;
        case HEADING_SOUTH:
            y++;
            if (y >= yMax) {
                y = yMax;
                heading = HEADING_EAST;
            }
            break;
        case HEADING_WEST:
            x--;
            if (x <= xMin) {
                x = xMin;
                heading = HEADING_SOUTH;
            }
            break;
        default:
            y--;
            if (y <= yMin) {
                y = yMin;
                heading = HEADING_WEST;
            }
    }
    return [x, y, heading];
}

const HEADING_NORTH = 1;
const HEADING_SOUTH = 2;
const HEADING_EAST = 3;
const HEADING_WEST = 4;
const HEAD = 0;
const X = 0;
const Y = 1;
const HEADING = 2;
const snake = [[2, 2, HEADING_NORTH]];
const MIN = 0;
const MAX = 1;
const RANGE_DEFAULT = 0;
const RANGE_CUSTOM = 1;
const RANGES = [[[0, 4], [0, 4]]];

let tempo = 200;
let size = 1;
let clockwise = true;
let range = RANGE_DEFAULT;
basic.forever(function () {
    basic.pause(tempo);
    basic.clearScreen();
    draw(snake);

    if (input.isGesture(Gesture.TiltLeft)) {
        clockwise = false;
    } else if (input.isGesture(Gesture.TiltRight)) {
        clockwise = true;
    }

    snake.unshift(forward(snake[HEAD], clockwise, RANGES[range]));
    while (snake.length > size) {
        snake.pop();
    }
})
