input.onButtonPressed(Button.A, function () {
    toggle(cursor[X], cursor[Y]);
    cursor[X]--;
    if (cursor[X] < 0) {
        cursor[X] = 4;
        cursor[Y]--;
    }
    if (cursor[Y] < 0) {
        cursor[Y] = 0;
        cursor[X] = -1;
    }
})
input.onButtonPressed(Button.B, function () {
    cursor[X]++;
    if (cursor[X] > 4) {
        cursor[X] = 0;
        cursor[Y]++;
    }
    if (cursor[Y] > 4) {
        cursor[Y] = 4;
        cursor[X] = 5;
    }
    toggle(cursor[X], cursor[Y]);
})
input.onButtonPressed(Button.AB, function () {
    reset();
})
input.onShake(function () {
    drawRandom();
})
input.onPinPressed(TouchPin.P0, function () {
    drawHeart();
})
function toggle(x: number, y: number) {
    if (leds[y][x] == 1) {
        leds[y][x] = 0;
    } else {
        leds[y][x] = 1;
    }
}
function draw() {
    for (let y = 0; y <= leds.length - 1; y++) {
        for (let x = 0; x <= leds[y].length - 1; x++) {
            if (leds[y][x] == 1) {
                led.plot(x, y);
            } else {
                led.unplot(x, y);
            }
        }
    }
}
function random() {
    let random = Math.floor(Math.random() * Math.floor(2));
    return random;
}
function setRow(index: number, row: number[]) {
    leds[index] = row;
}
function drawRandom() {
    for (let y = 0; y <= leds.length - 1; y++) {
        for (let x = 0; x <= leds[y].length - 1; x++) {
            leds[y][x] = random();
        }
    }
}
function drawHeart() {
    setRow(0, [0, 1, 0, 1, 0]);
    setRow(1, [1, 1, 1, 1, 1]);
    setRow(2, [1, 1, 1, 1, 1]);
    setRow(3, [0, 1, 1, 1, 0]);
    setRow(4, [0, 0, 1, 0, 0]);
}
function reset() {
    cursor[X] = 0;
    cursor[Y] = 0;
    setRow(0, [1, 0, 0, 0, 0]);
    setRow(1, [0, 0, 0, 0, 0]);
    setRow(2, [0, 0, 0, 0, 0]);
    setRow(3, [0, 0, 0, 0, 0]);
    setRow(4, [0, 0, 0, 0, 0]);
}
const X = 0;
const Y = 1;
const cursor = [0, 0];
const leds = [[1, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]];
basic.forever(function () {
    draw()
})
