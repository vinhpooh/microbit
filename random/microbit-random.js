input.onButtonPressed(Button.A, function () {
    tempo *= step;
})
input.onButtonPressed(Button.B, function () {
    tempo /= step;
    if (tempo <= 1) {
        tempo = 1;
    }
    console.log("" + tempo);
})
input.onButtonPressed(Button.AB, function () {
    if (step == 1.25) {
        step = 2;
    } else {
        step = 1.25;
    }
})
input.onShake(function () {
    tempo = 200;
    step = 1.25;
})
function draw() {
    for (let y = 0; y <= leds.length - 1; y++) {
        for (let x = 0; x <= leds[y].length - 1; x++) {
            if (leds[y][x] == 1) {
                led.plot(x, y);
            } else {
                led.unplot(x, y);
            }
        }
    }
}
function random() {
    let random = Math.floor(Math.random() * Math.floor(2));
    return random;
}
function drawRandom() {
    for (let y = 0; y <= leds.length - 1; y++) {
        for (let x = 0; x <= leds[y].length - 1; x++) {
            leds[y][x] = random();
        }
    }
}
let tempo = 200;
let step = 1.25;
const leds = [[1, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]];
basic.forever(function () {
    draw();
    basic.pause(tempo);
    drawRandom();
})
